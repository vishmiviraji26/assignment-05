#include <stdio.h>

#define Cost 500
#define CostAtt 3

int TicketPrice ;
int nmbOfAttendee(int TicketPrice);
int Income(int TicketPrice);
int Expenditure(int TicketPrice);
int Profit(int TicketPrice);

int nmbOfAttendee(int TicketPrice){
return 120+4*(15-TicketPrice);
}
int Income(int TicketPrice){
return TicketPrice*nmbOfAttendee(TicketPrice);
}
int Expenditure(int TicketPrice){
return CostAtt*nmbOfAttendee(TicketPrice)+Cost;
}
int Profit(int TicketPrice){
return Income(TicketPrice)-Expenditure(TicketPrice);
}
int main(){
int price;
printf("\n expected profit for ticket price: \n");
for (TicketPrice=1; TicketPrice<48 ; TicketPrice+=1)
{
    printf ("TicketPrice= rs%d \t\t profit=rs. %d\n", TicketPrice, Profit(TicketPrice));
    printf("\n");
}
return 0;
}

